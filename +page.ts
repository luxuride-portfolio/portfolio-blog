import type { PageLoad } from './$types';
import { getPosts } from '$lib/api/posts/get';

export const load: PageLoad = async ({ parent }) => {
	const { supabase, queryClient } = await parent();
	await queryClient.prefetchQuery({
		queryKey: ['posts'],
		queryFn: async () => await getPosts(supabase)
	});
};
