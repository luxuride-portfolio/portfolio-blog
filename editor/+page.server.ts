import { postPost } from '$lib/api/posts/post';
import type { Actions } from './$types';
import { error, redirect } from '@sveltejs/kit';

export const actions = {
	default: async (event) => {
		const session = await event.locals.getSession();
		if (!session) throw error(403);
		const formData = await event.request.formData();
		const title = formData.get('title')?.toString();
		const keywordsText = formData.get('keywords')?.toString();
		const description = formData.get('summary')?.toString();
		const body = formData.get('data')?.toString();
		if (
			formData === undefined ||
			title === undefined ||
			keywordsText === undefined ||
			description === undefined ||
			body === undefined ||
			title === '' ||
			keywordsText === '' ||
			description === '' ||
			body === ''
		) {
			throw error(422);
		}
		const keywords = keywordsText.split(',').map((x) => x.trim()) ?? [];
		const post = {
			title,
			keywords,
			description,
			body
		};
		const res = await postPost(event.locals.supabase, post).select().single();
		if (!res.data?.id) {
			throw error(res.status, res.error?.message);
		}
		throw redirect(303, `/blog/read/${res.data?.id}`);
	}
} satisfies Actions;
