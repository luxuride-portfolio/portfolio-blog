import { error } from '@sveltejs/kit';
import type { LayoutLoad } from './$types';
import { PUBLIC_SCHOOL } from '$env/static/public';

export const load: LayoutLoad = async ({ parent }) => {
	const { session, supabase } = await parent();
	if (PUBLIC_SCHOOL == 'true') return;
	if (!session) throw error(403);
	if (!(await supabase.rpc('is_admin')).data && !(await supabase.rpc('is_poster')).data)
		throw error(401);
};
