import { redirect, type Actions, error } from '@sveltejs/kit';

export const actions = {
	default: async (event) => {
		const session = await event.locals.getSession();
		if (!session) throw error(403);
		const postId = event.params.post;

		const res = await event.locals.supabase.from('posts').delete().eq('id', postId);
		if (res.error) throw error(401);
		throw redirect(303, `/blog/editor/${postId}/delete/deleted`);
	}
} satisfies Actions;
