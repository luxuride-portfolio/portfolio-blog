import type { PageLoad } from './$types';
import { getPost } from '$lib/api/posts/get';

export const load: PageLoad = async ({ parent, params: { post } }) => {
	const { supabase, queryClient } = await parent();
	await queryClient.prefetchQuery({
		queryKey: ['post', post],
		queryFn: async () => await getPost(supabase, post)
	});
};
