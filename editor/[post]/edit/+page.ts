import type { PageLoad } from './$types';
import { getPostRaw } from '$lib/api/posts/get';

export const load: PageLoad = async ({ parent, params: { post } }) => {
	const { supabase } = await parent();
	return {
		post: await getPostRaw(supabase, post)
	};
};
