import type { PageLoad } from './$types';
import { getPostRaw } from '$lib/api/posts/get';
import { error } from '@sveltejs/kit';

export const load: PageLoad = async ({ parent, params: { post } }) => {
	const { supabase, queryClient } = await parent();
	const displayPost = await getPostRaw(supabase, post);
	if (displayPost.error) throw error(404);
	await queryClient.prefetchQuery({
		queryKey: ['post', post, 'raw'],
		queryFn: async () => displayPost
	});
};
