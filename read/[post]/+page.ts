import type { PageLoad } from './$types';
import { getPost } from '$lib/api/posts/get';
import { error } from '@sveltejs/kit';

export const load: PageLoad = async ({ parent, params: { post } }) => {
	const { supabase, queryClient } = await parent();
	const displayPost = await getPost(supabase, post);
	if (displayPost.error) throw error(404);
	await queryClient.prefetchQuery({
		queryKey: ['post', post],
		queryFn: async () => displayPost
	});
};
